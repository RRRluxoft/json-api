package com.topjavatutorial;

import javax.json.Json;
import javax.json.JsonObject;

/**
 * Date: 27.02.2017
 *
 * @author Roman Litvishko
 */
public class JAXPExample {

    public static void main(String[] args) {
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("empName", "John Doe")
                .add("empId", "101")
                .add("age", "25")
                .build();

        System.out.println(jsonObject.toString());
    }
}
