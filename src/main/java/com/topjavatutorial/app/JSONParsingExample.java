package com.topjavatutorial.app;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParser.Event;
import java.io.StringReader;

/**
 * Date: 27.02.2017
 *
 * @author Roman Litvishko
 */
public class JSONParsingExample {

    public static void main(String[] args) {
        String jsonString = "{\"name\":\"John Doe\",\"employeeId\":\"101\",\"age\":\"25\"}";
        JsonParser parser = Json.createParser(new StringReader(jsonString));

        while (parser.hasNext()) {
            Event event = parser.next();
            if (event == Event.KEY_NAME) {
                if (isNameField(parser, "name")) {
                    parser.next();
                    System.out.println("Name: " + parser.getString());
                }
                if (isNameField(parser, "employeeId")) {
                    parser.next();
                    System.out.println("EmployeeId: " + parser.getString());
                }
                if (isNameField(parser, "age")) {
                    parser.next();
                    System.out.println("Age: " + parser.getString());
                }
            }
        }
    }

    private static boolean isNameField(JsonParser parser, String fieldName) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(fieldName), "check field ");
        return parser.getString().equals(fieldName);
    }

}
